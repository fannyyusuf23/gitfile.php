
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Git</h1>
    
    <?php

    	echo " <h3>Soal 1</h3>";
		function tentukan_nilai($number)
		{
		    if ($number >= 90 )
		    {
		    	echo " Sangat Baik <br> ";
		    }else if ($number >= 70)
		    {
		    	echo "Baik <br>";
		    }else if($number >= 60)
		    {
		    	echo " Cukup  <br>";
		    }else if($number >= 40)
		    {
		    	echo "Kurang <br>";
		    }
		}

		//TEST CASES
		echo tentukan_nilai(98); //Sangat Baik
		echo tentukan_nilai(76); //Baik
		echo tentukan_nilai(67); //Cukup
		echo tentukan_nilai(43); //Kurang

		echo "<h3>SOAL 2</h3>";



		function ubah_huruf($string){
		//kode di sini
		$abjad = "abcdefghijklmnopqrstuvwxyz";
		$output = "";
		for($i=0; $i < strlen($string); $i++  ){
			$position = strpos($abjad, $string[$i]);
			$output .= substr($abjad, $position + 1 , 1);
		}
		return $output . "<br>";
		}

		// TEST CASES
		echo ubah_huruf('wow'); // xpx
		echo ubah_huruf('developer'); // efwfmpqfs
		echo ubah_huruf('laravel'); // mbsbwfm
		echo ubah_huruf('keren'); // lfsfo
		echo ubah_huruf('semangat'); // tfnbohbu

		echo "<h3>SOAL 3 </h3>";

		function tukar_besar_kecil($string){

		$str="";

		 for ($i=0; $i < strlen($string); $i++)
		 {
		 	if(ctype_upper($string[$i]))
		 	{
		 		$str .= strtolower($string[$i]);

		 	}else
		 	{
		 		$str .= strtoupper($string[$i]);
		 	}
		 }
		 	return $str . "<br>";
		}

		// TEST CASES
		echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
		echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
		echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
		echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
		echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

	?>
</body>

</html>